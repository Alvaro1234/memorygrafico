
package memorygrafico;

import java.util.Random;

/**
 *
 * @author Javier Fernández
 */
public class Juego {
    private static String secuencia = "";
    private static int dificultad = 12;

    //Creamos un método para obtener al secuencia
    public static void generaPartida(){
        secuencia = "";
        Random aleatorio = new Random();
        while(secuencia.length() < dificultad){
            byte repeticion = 0;

            int valor = aleatorio.nextInt(dificultad/2) + 65;
            char caracter = (char)valor;

            //Realiza la comparación recorriendo la cadena
            int posicion =secuencia.length()-1;
            for (byte orden = (byte)posicion; orden >= 0; orden --) {
                
                if (caracter == secuencia.charAt(orden)){
                    repeticion++;
                }
            }                
            
            //En el caso de que ya haya el máximo de repetidos no agrega el 
            // caracter
            byte repeticionMax = 2;
            if (repeticion != repeticionMax){
                secuencia = secuencia.concat(String.valueOf(caracter));
            }
        }
        
    }
    
    //Creamos un método para que se pueda coger la secuencia
    public static String getSecuencia(){
        return secuencia;
    }
    
    //Creamos un método para que se pueda coger la dificultad
    public static int getDificultad(){
        return dificultad;
    }
    
}
