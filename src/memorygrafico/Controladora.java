
package memorygrafico;

import javafx.scene.image.ImageView;

public class Controladora {  
    
    // creamos una variable 
    static String nombreCartas;
    
    // creamos la variable para guardar la secuencia
    String secuencia;    
//    static private ImageView imagen;
    
    // creamos un metodo para comprar la secuencia con cada carta
    public void setCarta(){
        int dificultad = Juego.getDificultad();
        for (int i=0; i<secuencia.length(); i++){
            char caracterActual = secuencia.charAt(i);            
            Imagenes carta = new Imagenes();
            ImageView imageViewCarta = carta.cogeImageView(caracterActual);
            //MemoryGrafico.muestraCarta();
            MemoryGrafico.muestraCarta(imageViewCarta);
        }  
    }
    
//    public static ImageView getImagen(){
//        return imagen;
//    }
    
    /* Creamos un objeto PanelMarcador al que llamamos marcador, el que utilizaremos 
    para llamar a metodos creados en la clase PanelMarcador*/
    private static PanelMarcador marcador = new PanelMarcador(); 
    
    /* Creamos el metodo que llama a metodos creados en la clase PanelMarcador
    y que incrementara el marcador del jugador*/
    public void incrementarMarcador(){
        marcador.incrementarContador();
        int puntuacionActualJuego = marcador.getMarcador();
    }
    
    /* Creamos un metodo que le diga a la clase Juego que genere la secuencia 
    cuando se halla pulsado la el boton inicio*/
    public void startGame(){
//        MemoryGrafico.limpiarZonaJuego();
        Juego.generaPartida();
        secuencia = Juego.getSecuencia(); 
        setCarta();
    }   
}
