
package memorygrafico;

import javafx.scene.image.Image;
import javafx.scene.image.ImageView;

/**
 *
 * @Javier Fernández
 */
public class Imagenes {
    
    private Image imagen;
    private ImageView imageViewElement;
    
    // Creamos un método que asigna el nombre de la imagen que se a coger y 
    // retorna la imagen para que pueda ser utilizada
    public ImageView cogeImageView(char numImagen) {
        imagen = new Image(getClass().getResourceAsStream("/images/cara/" + 
                numImagen + ".png"));
        imageViewElement = new ImageView();
        imageViewElement.setImage(imagen);
        return imageViewElement;
    }
    
}
