
package memorygrafico;

import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.Label;
import javafx.scene.layout.HBox;


public class PanelMarcador extends HBox{
    // creamos un objeto label para el mrcador
    public Label labelMarcador = new Label("0");   
    // Creamos un nuevo objeto label para poder asignar el nombre del jugador
    public Label labelPlayerName = new Label();
    // creamos la variable que va a contener en todo momento la puntuacion
    public int puntuacionMarcador = 0;
    // creamos la variable para la puntuacion del juego
    int puntuacionJuego = 0;
    
    
    public PanelMarcador (){       
        // aplicamos estilos a labelPlayerName
        labelPlayerName.setStyle("-fx-text-aligment: left;"
            + "-fx-font-size: 24;" 
            + "-fx-text-alignment: center;"
            + "-fx-font-weight: bold;"
            + "-fx-label-padding: 10 50 0 0");

        
        // aplicamos estilos al label de labelMarcador
        labelMarcador.setStyle("-fx-text-aligment: right;"
            + "-fx-font-size: 24;"
            + "-fx-font-weight: bold;"
            + "-fx-label-padding: 10,0,0,0");
        
        // añadimos los dos label a la ventana
        this.getChildren().add(labelPlayerName);
        this.getChildren().add(labelMarcador);
        // colocamos los label dentro del contenedor
        this.setAlignment(Pos.TOP_LEFT);
        this.setPadding(new Insets(10, 10, 10, 10));        
    }
    
    // creamos un metodo para regoger la puntuacion en el label
    public void setPuntuacion(int puntuacion){
        labelMarcador.setText(String.valueOf(puntuacion));
    }
    
    // creamos un metodo para regoger el nombre del jugador
    public void setPlayerName (String playerName){
        labelPlayerName.setText(playerName);
    }    
   
    /* Creamos el metodo al que va a llamar la clase PanelMarcador para incrementar
    la puntuacion que se va a mostrar en pantalla*/
    public void incrementarContador(){
        puntuacionJuego ++;
    }
    
    /* Creamos el metodo al que va a llamar la clase PanelMarcador para que
    devuelva el marcador con al puntuacion inicial*/
    public int getMarcador(){
        return puntuacionMarcador;
    }
}
    

