
package memorygrafico;

import javafx.application.Application;
import static javafx.application.Application.launch;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Pos;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.image.ImageView;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.FlowPane;
import javafx.scene.paint.Color;
import javafx.stage.Stage;

/**
 *
 * @Javier Fernández
 */
public class MemoryGrafico extends Application {
    //Creamos un panel flow
    static FlowPane flow = new FlowPane();
    
    @Override
    public void start(Stage primaryStage) {
    //Le asignamos un título a la ventena
    primaryStage.setTitle("Memory");
    Group root = new Group();
    
    //Las medida que va a tener la ventana
    double width = 900;
    double height = 700;
    
    // Define el tamano que va a tener según el número de cartas
    double flowWidth = Juego.getDificultad() / (Math.floor(Math.sqrt(Juego.
            getDificultad()))) * 65 + Juego.getDificultad() / (Math.floor(Math.
                    sqrt(Juego.getDificultad())))* 10 * 2;
    
    //Creamos la escena
    Scene scene = new Scene(root, width, height, Color.WHITE);
    
    //Creamos un BorderPane y le asignamos el mismo tamaño de la escena
    BorderPane borderPane = new BorderPane();
    borderPane.setPrefSize(width, height);
    
    //En la sección central de BorderPane le introducimos el FlowPane creado
    // fuera lo alineamos, le asignamos el espacio que va a haber entre los 
    // diferentes objetos que va a contener y la anchura maxima
    borderPane.setCenter(flow);
    flow.setAlignment(Pos.CENTER);
    flow.setHgap(10);
    flow.setVgap(10);
    flow.setMaxWidth(flowWidth);
    
    //Insertamos el marcador
    PanelMarcador marcador = new PanelMarcador();
    marcador.setPlayerName("Alvaro");
    marcador.setPuntuacion(10);
    
    //Insertamos un FlowPane el la parte superior del BorderPane para insertar
    // el marcador
    FlowPane flowMarcador = new FlowPane();
    borderPane.setTop(flowMarcador);
    flowMarcador.setMinHeight(60);
    flowMarcador.setAlignment(Pos.CENTER);
    flowMarcador.getChildren().add(marcador);
    
    //Creamos un botón y le asignamos los metodos a los que va a llamar
    Button botonInicio = new Button("Iniciar partida");
        botonInicio.setOnAction(new EventHandler<ActionEvent>() {            
            @Override
            public void handle(ActionEvent event) {
                flow.getChildren().clear();
                Controladora iniciar = new Controladora();
                iniciar.startGame();
            }
        });
    
    //Insertamos un FlowPane el la parte inferior del BorderPane para insertar
    // el botón    
    FlowPane flowBoton = new FlowPane();
    flowBoton.setAlignment(Pos.CENTER);
    borderPane.setBottom(flowBoton);
    flowBoton.setMinHeight(60);
    flowBoton.getChildren().add(botonInicio);
    
    //Introducimos el BorderPane dentro de la escena
    root.getChildren().add(borderPane);
    primaryStage.setScene(scene);
    primaryStage.show();
    
    }
 
    public static void main(String[] args) {
        launch(args);
    }
    
    //Creamos un metodo para insertar las imagenes de las cartas dentro de la
    // zona de juego
    public static void muestraCarta(ImageView imageView) {
        flow.getChildren().add(imageView);
    }
    
}
