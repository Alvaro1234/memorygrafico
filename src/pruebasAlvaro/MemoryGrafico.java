/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pruebasAlvaro;

import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.layout.StackPane;
import javafx.stage.Stage;
import memorygrafico.PanelMarcador;

/**
 *
 * @author 1DAW17
 */
public class MemoryGrafico extends Application {
  
   
    public void start(Stage primaryStage) {
        // creamos el objeto marcador
        PanelMarcador marcador = new PanelMarcador();
        marcador.setPlayerName("Alvaro");
        marcador.setPuntuacion(10);
        
        // creamos un root y añadimos el marcador
        StackPane root = new StackPane();
        root.getChildren().add(marcador);
       
        
        Scene scene = new Scene(root, 300, 250);
        
        primaryStage.setTitle("Hello World!");
        primaryStage.setScene(scene);
        primaryStage.show();
        
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        launch(args);
    }
    
}
